// Online C++ compiler to run C++ program online
#include <iostream>

using namespace std;

int main() {
    int i,j,k,l;
      cout << "Masukkan jumlah baris: ";
      cin >> k;
      for (i = 0; i <= k; i++)
      {
            for (l = k; l > i; l--)
                  cout << " ";
            for (j = 0; j < i; j++)
                  cout << "* ";
            cout << "\n";
      }
      for (i = 1; i < k; i++)
      {
            for (l = 0; l < i; l++)
                  cout << " ";
            for (j = k; j > i; j--)
                  cout << "* ";
            // ending line after each row
            cout << "\n";
      }
    
    return 0;
}
